from utils.instagram_scraper import scrape, login, init_webdriver
from utils.general_classifier import resnet_is_class as check_for
from utils.external import detect_fire_and_guns
from utils import download_file
from shutil import copyfile
from PIL import Image
import cv2
import re
import json


FS_PATH = '_data'
TEST_SEARCH_PATH = f'{FS_PATH}/test_search'

# modes 
# GENERAL: image classifier search w/resnet50
# GUN_DETECT: custom model for gun detection


def scan_guns(data, post, media_type, file_ext, file_path, iter):
    url = post['media'][iter]
    file_path = f'{TEST_SEARCH_PATH}/posts/{post["id"]}_{iter}.{file_ext}'

    print(f'\text: {file_ext}\ttype: {media_type}\n\tpath: {file_path}')
    download_file(file_path, url)
    results = detect_fire_and_guns(file_path, media_type=media_type)

    if (len(results) > 0):
        is_match = True
        data['matches'].append(r[1] for r in results)
        print('FOUND MATCH')
        for r_index in range(len(results)):
            labeled_image, classes = results[r_index]
            print(f'\t{classes}')
            match_path = f'{TEST_SEARCH_PATH}/matches/{post["id"]}_{iter}_r{r_index}.jpeg'
            cv2.imwrite(match_path, labeled_image)
            with Image.open(match_path) as show_image:
                show_image.show()

def scan_general(data, post, media_type, file_ext, file_path, iter, expected_class):
    url = post['media'][iter]
    file_path = f'{TEST_SEARCH_PATH}/posts/{post["id"]}_{iter}.{file_ext}'

    print(f'\text: {file_ext}\ttype: {media_type}\n\tpath: {file_path}')
    download_file(file_path, url)

    match = check_for(file_path, expected_class)
    if (match):
        match_path = f'{TEST_SEARCH_PATH}/matches/{post["id"]}_{iter}.jpeg'
        print('FOUND MATCH')
        copyfile(file_path, match_path)
        with Image.open(match_path) as show_image:
            show_image.show()


# Recursive profile scraper
def scan(profile, search_depth, root_depth, driver, result_dump=None, mode='GENERAL', expected_class='golden retriever'):
    print(f'SCANING PROFILE: {profile}')

    if not result_dump:
        result_dump = {}

    for k in result_dump.keys():
        if profile == k:
            return

    data = scrape(profile, driver, follower_limit=0, post_limit=30)
    if (not data):
        return

    data['matches'] = []

    is_match = False
    for post in data['posts']:
        print(f'Evaluating media for post: {post["id"]}')
        for i in range(len(post['media'])):
            url = post['media'][i]

            ext_match = re.search(r'(?<=\.)\w+(?=\?|$)', url)
            file_ext = ext_match.group(0) if ext_match else ''
            media_type = 'video' if file_ext == 'mp4' else 'image'

            # Placeholder for avoiding video processing
            if (media_type == 'video'):
                continue

            file_path = f'{TEST_SEARCH_PATH}/posts/{post["id"]}_{i}.{file_ext}'

            print(f'\text: {file_ext}\ttype: {media_type}\n\tpath: {file_path}')

            if (mode == 'GENERAL'):
                scan_general(data, post, media_type, file_ext, file_path, i, expected_class)
            else:
                scan_guns(data, post, media_type, file_ext, file_path, i)

            result_dump[profile] = data

    if is_match and root_depth > 0:
        next_scan_depth = root_depth
    elif (search_depth > 0):
        next_scan_depth = search_depth - 1
    else:
        return

    for p in data['followers']:
        scan(p, next_scan_depth, root_depth, driver, result_dump)

    return result_dump


if (__name__ == "__main__"):
    with open('../.media/logo.txt') as logo:
        print(logo.read())

    login_data = open('login_data.txt', 'r')
    username = login_data.readline().strip('\n')
    password = login_data.readline().strip('\n')
    login_data.close()

    driver = init_webdriver()
    login(username, password, driver)
    mode_select = input('SELECT MODE \n\t1: for GENERAL IMAGE CLASSIFIER\n\t2: for GUN DETECTION\n\t3: for TEST RUN\n\nmode: ')
    profile = input('SELECT PROFILE TO SCRAPE: ')
    if (mode_select == '1'):
        expected_class = input('INSERT CLASSIFIER FILTER: ')
        dump = scan(profile, 0, 0, driver, mode='GENERAL', expected_class=expected_class)
    elif (mode_select == '2'):
        dump = scan(profile, 0, 0, driver, mode='GUN_DETECTION')
    else:
        dump = scan('dogsitter.cl', 0, 0, driver, mode='GENERAL', expected_class='Pomeranian')
    data_file = open(f'{TEST_SEARCH_PATH}/data.json', 'w')
    json.dump(dump, data_file, indent=4)

    driver.close()
    data_file.close()
