from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
from utils.json_parser import find_value_in_json, find_values_in_json
import json


def login(username, password, driver):
    driver.get('https://instagram.com')
    wait = WebDriverWait(driver, 10)

    username_input = wait.until(
        EC.presence_of_element_located((By.NAME, 'username'))
    )
    username_input.click()
    username_input.send_keys(username)

    password_input = driver.find_element(By.NAME, 'password')
    password_input.click()
    password_input.send_keys(password)

    driver.find_element(
        By.XPATH,
        '//div[text()="Log in"]'
    ).click()

    try:
        wait.until(
            EC.presence_of_element_located((
                By.XPATH,
                '//div[text()="Not Now"]'
            ))
        ).click()
    except Exception:
        pass

    try:
        wait.until(
            EC.presence_of_element_located((
                By.XPATH,
                '//button[text()="Not Now"]'
            ))
        ).click()
    except Exception:
        pass

    sleep(1)


def scrape_hashtag(tag):
    pass


def scrape_followers(username, driver, limit=200):
    print(f'navigating to: https://instagram.com/{username}')
    driver.get(f'https://instagram.com/{username}')

    wait = WebDriverWait(driver, 10)
    max_scrolls = limit//12 if (limit % 12 == 0) else (limit//12) + 1

    wait.until(
        EC.presence_of_element_located((By.XPATH, '//a[text()=" followers"]'))
    ).click()

    # Wait until modal open
    wait.until(
        EC.presence_of_element_located((By.XPATH, '//div[@class="_aano"]'))
    )
    for i in range(max_scrolls):
        driver.execute_script("""
            let scroller = document.querySelector('._aano');
            scroller.scrollTop = scroller.scrollHeight
        """)
        sleep(2.3)

    folower_elements = driver.find_elements(
        By.XPATH,
        '//div[@class="_aano"]/div/div/div/div/div/div\
        /div[2]/div/div/div/div/div/a/div/div/span'
    )

    return [s.text for s in folower_elements]


def get_post_media_src(url, driver):
    driver.get(url)
    scripts = driver.find_elements(
        By.XPATH,
        '//script[@type="application/json"]'
    )

    json_data = []
    for script in scripts:
        content = json.loads(script.get_attribute('innerHTML'))
        data = find_value_in_json(
            content,
            'xdt_api__v1__media__shortcode__web_info'
        )
        if not data:
            continue

        img_data = find_values_in_json(
            data,
            'image_versions2'
        )
        video_data = find_values_in_json(
            data,
            'video_versions'
        )
        if (not img_data) or (not video_data):
            continue

        if len(img_data) == 1:
            json_data = [video_data[0]] if video_data[0] else [img_data[0]['candidates']]
            break

        for i in range(1, len(img_data)):
            if (video_data[i]):
                json_data.append(video_data[i])
            else:
                json_data.append(img_data[i]['candidates'])

    urls = [j[0]['url'] for j in json_data]
    return urls


def scrape_posts(username, driver, limit=None):
    print(f'scraping {username} posts')
    driver.get(f'https://instagram.com/{username}')
    wait = WebDriverWait(driver, 10)
    posts_text = wait.until(
        EC.presence_of_element_located((By.XPATH, '//ul/li/span/span'))
    ).text
    posts = int(posts_text.replace(',', ''))
    print(f'TOTAL POSTS: {posts}\nLIMIT SET AT: {limit}')

    if (not limit):
        max_posts = posts
    else:
        max_posts = min(limit, posts)
    print(f'will scrape {max_posts} posts')

    n_posts = 0

    print(f'MAX POSTS: {max_posts}')
    while (n_posts < max_posts):
        driver.execute_script('window.scrollTo(0, document.body.scrollHeight)')
        posts = driver.find_elements(
            By.XPATH,
            '//article/div/div/div/div/a'
        )
        n_posts = len(posts)
        sleep(2)
        print(f'n° post update: {n_posts}')

    hrefs = []
    for i in range(max_posts):
        try:
            hrefs.append(posts[i].get_attribute('href'))
        except Exception:
            continue

    results = [
        {
            'id': url.strip('/').split('/')[-1],
            'media': get_post_media_src(url, driver)
        } for url in hrefs
    ]
    return results


def get_story_media_src(url, driver):
    driver.get(url)
    scripts = driver.find_elements(
        By.XPATH,
        '//script[@type="application/json"]'
    )

    json_data = []
    for script in scripts:
        content = json.loads(script.get_attribute('innerHTML'))
        data = find_value_in_json(
            content,
            'xdt_api__v1__feed__reels_media__connection'
        )
        parsed_data = find_values_in_json(data, 'video_versions')

        if (parsed_data and len(parsed_data) > 0):
            json_data = parsed_data
            break

    urls = [j[0]['url'] for j in json_data]
    return urls


def scrape_stories(user_id, driver):
    pass


def user_is_private(user_id, driver):
    driver.get(f'https://www.instagram.com/{user_id}/')
    try:
        wait = WebDriverWait(driver, 10)

        wait.until(
            EC.presence_of_element_located((
                By.XPATH,
                '//h2[text()="This Account is Private"]'
            ))
        )
        return True
    except Exception:
        return False


def scrape(user_id, driver, post_limit=40, follower_limit=200):
    if (user_is_private(user_id, driver)):
        return None

    if (follower_limit > 0):
        followers = scrape_followers(user_id, driver, limit=follower_limit)
    else:
        followers = []

    if (post_limit > 0):
        posts = scrape_posts(user_id, driver, limit=post_limit)
    else:
        posts = []

    scrape_stories(user_id, driver)

    return {
            "followers": followers,
            "posts": posts
    }


def init_webdriver():
    driver = webdriver.Firefox()
    return driver
