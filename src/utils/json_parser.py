def find_value_in_json(json_obj, target_key):
    if isinstance(json_obj, dict):
        for key, value in json_obj.items():
            if key == target_key:
                return value
            elif isinstance(value, (dict, list)):
                result = find_value_in_json(value, target_key)
                if result is not None:
                    return result
    elif isinstance(json_obj, list):
        for item in json_obj:
            result = find_value_in_json(item, target_key)
            if result is not None:
                return result
    return None


def find_values_in_json(json_obj, target_key):
    results = []

    def search(json_obj):
        if isinstance(json_obj, dict):
            for key, value in json_obj.items():
                if key == target_key:
                    results.append(value)
                elif isinstance(value, (dict, list)):
                    search(value)
        elif isinstance(json_obj, list):
            for item in json_obj:
                search(item)

    search(json_obj)
    return results
