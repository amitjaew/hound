import requests

def download_file(path, url):
    r = requests.get(url)
    f = open(path, 'wb')

    for chunk in r.iter_content(chunk_size=255):
        if chunk:
            f.write(chunk)
    f.close()
