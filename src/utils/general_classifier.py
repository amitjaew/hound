import tensorflow as tf
from tensorflow import keras
from PIL import Image
import numpy as np

tf.keras.backend.clear_session()
resnet50 = keras.applications.resnet50.ResNet50(weights="imagenet")


def resnet_classify(im_path):
    im = Image.open(im_path)
    im = np.array([im])
    im_resize = tf.image.resize(im, [224, 224])
    inputs = keras.applications.resnet50.preprocess_input(im_resize)

    probs = resnet50.predict(inputs)
    predictions = keras.applications\
        .resnet50.decode_predictions(
            probs, top=5
        )[0]
    print(predictions)
    return predictions


def resnet_is_class(im_path, expected_class, min_conf=0.5):
    prediction = resnet_classify(im_path)
    for p in prediction:
        id, class_detected, conf = p
        if (conf < min_conf):
            break
        elif (class_detected == expected_class.replace(' ', '_')):
            return True
    return False


if (__name__ == '__main__'):
    is_retriever = resnet_is_class(
        'test_images/test4.jpg',
        'golden retriever',
        0.5
    )
    print(is_retriever)
